<?php

namespace Drupal\config_batch_export\Config;

use Drupal\Core\Config\CachedStorage;

/**
 * Class Storage.
 *
 * @package Drupal\config_batch_export\Config
 */
class Storage extends CachedStorage {}